var dados = [
    {disciplina : "Introdução à programação", n1 : 10, n2: 7, n3: 9, n4: 6},
    {disciplina : "Banco de dados", n1 : 0, n2: 3, n3: 9, n4: 2},
    {disciplina : "Algoritmos",  n1 : 0, n2: 1, n3: 2, n4: 3},
    {disciplina : "Desenvolvimento Web",  n1 : 10, n2: 7, n3: 9, n4: 6},
    {disciplina : "Cálculo 1", n1 : 10, n2: 7, n3: 9, n4: 6},
    {disciplina : "Geometria analítica", n1 : 10, n2: 7, n3: 9, n4: 6},
    {disciplina : "Algebra linear", n1 : 10, n2: 7, n3: 9, n4: 6},
    {disciplina : "Estrutura de dados", n1 : 10, n2: 7, n3: 9, n4: 6},
];

module.exports = function(site){
    site.get('/', function(req, res){
        res.render('index', {dados});
    })
    
    site.get('/aluno', function(req, res){
        res.end('Gabriel Nascimento')
    })
    
    site.get('/curso', function(req, res){
        res.end('Programador Web')
    })
}